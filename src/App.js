import './App.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import MainHome from "./components/MainHome";
import MainUniversity from "./components/MainUniversity";
import MainFacultyMember from "./components/MainFacultyMember";


function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<MainHome/>}/>
                <Route path="university" element={<MainUniversity/>}/>
                <Route path="/:nameId" element={<MainFacultyMember/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
