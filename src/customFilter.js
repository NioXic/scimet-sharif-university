function persianDigit(value){
    let str = value+'';
    return str.split('0').join('۰')
        .split('1').join('۱')
        .split('2').join('۲')
        .split('3').join('۳')
        .split('4').join('۴')
        .split('5').join('۵')
        .split('6').join('۶')
        .split('7').join('۷')
        .split('8').join('۸')
        .split('9').join('۹')
}


export default{
    persianDigit,
}