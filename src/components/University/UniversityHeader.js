import {Row, Col, Container, Button} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import {useNavigate} from "react-router-dom";

import logo from '../../assets/secondLogo.png'
import emblem from '../../assets/emblem.png'
//styles
import '../../App.css'
import globalStyles from "../globalStyles";

function UniversityHeader() {
    const navigate = useNavigate();

    function pushToUniversityPage() {
        navigate("/university");
    }
    function secondHeaderCol(){
        return (
            <Col className={'secondHeaderCol'}>
                <Row>
                    <Col className={'col-12'}>
                                <span style={{fontSize: "1.5rem", color: 'blue', fontWeight: "bold"}}>
                                    خلاصه علمکرد
                                </span>
                    </Col>
                    <Col className={'col-12'} style={{display: "flex", justifyContent: 'center'}}>
                                <span style={{
                                    fontSize:'1.1rem',
                                    background: '#555',
                                    color: 'white',
                                    borderRadius: '5px',
                                    width: '40%',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: "center"
                                }}>
                                    43412
                                </span>
                    </Col>
                </Row>
            </Col>
        );
    }


    return (
        <div style={{width: "100%"}}>
            <div style={{width: "80%", margin: "auto", paddingBottom: "20px"}}>
                <Row className={'col-12'} style={{paddingTop: '15px', marginRight: "10px"}}>
                    <Col className={'col-6 d-flex justify-content-start align-items-center'}>
                        <img src={logo} className={'emblemClass'} style={{marginLeft :'15px'}}/>
                        <Row>
                            <Col className={'col-12 d-flex justify-content-start mottoTitle align-items-end'}>
                                <span>
                                    دانشگاه صنعتی شریف
                                </span>
                            </Col>
                            <Col className={'col-12 d-flex justify-content-start mottoTitle'}>
                                <span style={{color: "gray", fontSize: "1.5rem"}}>
                                    سامانه علم سنجی دانشگاه
                                </span>
                            </Col>
                        </Row>
                    </Col>
                    <Col className={'col-6 d-flex justify-content-end align-items-center'}>
                        <Row style={{marginLeft: '5px'}}>
                            <Col className={'col-12 d-flex justify-content-end mottoTitle align-items-end'} style={{textAlign:'end'}}>
                                <span style={{fontWeight: "500"}}>
                                    Sharif University of Technology
                                </span>
                            </Col>
                            <Col className={'col-12 d-flex justify-content-end mottoTitle'} style={{textAlign:'end'}}>
                                <span style={{color: "gray", fontSize: "1.5rem"}}>
                                    University Scientometrics Information Database
                                </span>
                            </Col>
                        </Row>
                        <img src={emblem}  className={'emblemClass'}/>
                    </Col>
                </Row>
            </div>
            <Row className={'col-12 secondHeaderSection d-flex justify-content-center'} style={{margin: "auto"}}>
                <Row style={{width: "80%"}}>
                    {secondHeaderCol()}
                    {secondHeaderCol()}
                    {secondHeaderCol()}
                    {secondHeaderCol()}
                    {secondHeaderCol()}
                    {secondHeaderCol()}
                    {secondHeaderCol()}
                    {secondHeaderCol()}
                </Row>
            </Row>
        </div>
    );
}

export default UniversityHeader;


//Styles
const logoColumn = {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",

};