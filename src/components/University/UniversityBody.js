import {Col, FormControl, FormText, Row} from "react-bootstrap";
import Trend from 'react-trend';
import {CircularProgressbar, buildStyles} from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import researchFieldSvg from "./Svg";
import EPRight from '../../assets/EPRight.png'
import EPLeft from '../../assets/EPLeft.png'
import avatar from '../../assets/avatar.jpg'
import noLogo from '../../assets/noLogo.jpg'
import {useState} from "react";
import {UserData} from "./Data";
import LineChart from "../LineChart";
import Table from "react-bootstrap/Table";
import customFilter from "../../customFilter";
import HomeFooter from "../Home/HomeFooter";

function UniversityBody() {
    const eachMember = {
        name: 'بهزاد عطایی',
        rank: 'استاد',
        hIndex: 38,
        journals: 151,
        documents: 4866,
    };
    const facultyList = [eachMember, eachMember, eachMember, eachMember, eachMember];

    let whichJournalTab = 'journals';
    const [userData, setUserData] = useState({
        labels: UserData.map((data) => data.year),
        datasets: [
            {
                label: "تعداد مقالات",
                data: UserData.map((data) => data.userGain),
                backgroundColor: [
                    "rgba(227, 2, 85, 1)"
                ],
                borderColor: "rgba(227, 2, 85, 1)",
                borderWidth: 2,
            },
        ],
    });


    return (
        <div className={'d-flex justify-content-center'}
             style={{paddingTop: "30px", flexDirection: 'column'}}>
            <Row style={{width: "80%", margin: 'auto'}}>
                <Col className={'col-8 d-flex justify-content-start'}
                     style={{paddingRight: '30px', paddingBottom: "30px"}}>
                    <span style={{fontSize: '1.5rem', fontWeight: 'bold', paddingLeft: '15px'}}>
                        خلاصه عملکرد
                    </span>
                    <span className="radioGroup" data-group-name="collection">
                        <input type="radio" name="collection" id="collection_0" value="0"/>
                        <label htmlFor="collection_0">Scopus</label>
                            <input type="radio" name="collection" id="collection_1" value="2"/>
                            <label htmlFor="collection_1">ISI (WoS)</label>
                    </span>
                </Col>
                <Col className={'col-4 d-flex justify-content-end'} style={{paddingBottom: "30px"}}>
                    <Row>
                        <Col className={'col-12 d-flex justify-content-end'}>
                        <span style={{color: "#215B90", fontSize: "1.4rem", fontWeight: "bold"}}>
                            Scopus Profile
                        </span>
                        </Col>
                        <Col className={'col-12 d-flex justify-content-end'}>
                            <span style={{color: "#aaa", fontWeight: "bold", fontSize: "1.1rem"}}>
                                به روز شده در 1401/6/19
                            </span>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12'}>
                    <Row>
                        <Col className={'col-3'}
                             style={{borderLeft: '1px solid #e1e4e6', borderRight: '1px solid #e1e4e6'}}>
                            <Col className={'col-12'}>
                                <span style={{fontSize: '3rem', fontWeight: "bold"}}>
                                    38458
                                </span>
                                <br/>
                                <span style={{fontSize: "1.5rem"}}>
                                    مقالات
                                </span>
                            </Col>
                            <Col className={'col-12'}>
                                <Trend
                                    smooth
                                    autoDraw
                                    autoDrawDuration={1500}
                                    autoDrawEasing="ease-out"
                                    data={[0, 2, 5, 9, 5, 10, 3, 5, 0, 0, 1, 8, 2, 9, 0]}
                                    gradient={['#00c6ff', '#F0F', '#FF0']}
                                    radius={5}
                                    strokeWidth={3}
                                    strokeLinecap={'butt'}
                                />
                            </Col>
                        </Col>
                        <Col className={'col-3'}
                             style={{borderLeft: '1px solid #e1e4e6', borderRight: '1px solid #e1e4e6'}}>
                            <Col className={'col-12'}>
                                <span style={{fontSize: '3rem', fontWeight: "bold"}}>
                                    573622
                                </span>
                                <br/>
                                <span style={{fontSize: "1.5rem"}}>
                                    استنادات
                                </span>
                            </Col>
                            <Col className={'col-12'}>
                                <Trend
                                    smooth
                                    autoDraw
                                    autoDrawDuration={1500}
                                    autoDrawEasing="ease-out"
                                    data={[5, 0, 2, 3, 6, 10, 12, 5, 9, 19, 0, 9, 5]}
                                    gradient={['#00c6ff', '#F0F', '#FF0']}
                                    radius={5}
                                    strokeWidth={3}
                                    strokeLinecap={'butt'}
                                />
                            </Col>
                        </Col>
                        <Col className={'col-3'}
                             style={{borderLeft: '1px solid #e1e4e6', borderRight: '1px solid #e1e4e6'}}>
                            <Col className={'col-12'}>
                                <span style={{fontSize: '3rem', fontWeight: "bold"}}>
                                    191
                                </span>
                                <br/>
                                <span style={{fontSize: "1.5rem"}}>
                                    H-Index
                                </span>
                            </Col>
                            <Col className={'col-12'}>
                                <Trend
                                    smooth
                                    autoDraw
                                    autoDrawDuration={1500}
                                    autoDrawEasing="ease-out"
                                    data={[0, 2, 5, 6, 7, 7, 1, 3, 2, 15, 1, 8, 2, 9, 0]}
                                    gradient={['#00c6ff', '#F0F', '#FF0']}
                                    radius={5}
                                    strokeWidth={3}
                                    strokeLinecap={'butt'}
                                />
                            </Col>
                        </Col>
                        <Col className={'col-3'}
                             style={{borderLeft: '1px solid #e1e4e6', borderRight: '1px solid #e1e4e6'}}>
                            <Col className={'col-12'}>
                                <Row>
                                    <Col className={'col-6'}>
                                    <span style={{fontSize: '3rem', fontWeight: 'bold'}}>
                                        35
                                    </span>
                                        <br/>
                                        <span style={{fontSize: '1.25rem'}}>
                                        اعضای هیات علمی با صفر یا یک مقاله
                                    </span>
                                    </Col>
                                    <Col className={'col-6'}>
                                    <span style={{fontSize: '3rem', fontWeight: 'bold'}}>
                                        443
                                    </span>
                                        <br/>
                                        <span style={{fontSize: '1.25rem'}}>
                                        تعداد اعضای هیات علمی شاغل
                                    </span>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className={'col-6'}>
                                           <span style={{fontSize: '3rem', fontWeight: 'bold'}}>
                                        16
                                        <span style={{color: 'grey'}}>
                                            /45
                                        </span>
                                    </span>
                                        <br/>
                                        <span style={{fontSize: '1.25rem'}}>
                                 نسبت اسناد به مقاله در همه سال ها
                                    </span>
                                    </Col>
                                    <Col className={'col-6'}>
                                    <span style={{fontSize: '3rem', fontWeight: 'bold'}}>
                                        4
                                        <span style={{color: 'grey'}}>
                                            /94
                                        </span>
                                    </span>
                                        <br/>
                                        <span style={{fontSize: '1.25rem'}}>
                                            نسبت مقاله به هیات علمی در 2021
                                    </span>
                                    </Col>
                                </Row>
                            </Col>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12'} style={{paddingTop: '30px'}}>
                    <Row>
                        <Col className={'col-3 d-flex justify-content-center flex-column align-items-center'}>
                            <div style={{width: '50%', fontWeight: 'bold'}}>
                                <CircularProgressbar
                                    value={28}
                                    text={`${28}%`}
                                    background
                                    styles={buildStyles({
                                        // Rotation of path and trail, in number of turns (0-1)
                                        rotation: 1,

                                        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                                        strokeLinecap: 'butt',

                                        // Text size
                                        textSize: '1.8rem',
                                        // How long animation takes to go from one percentage to another, in seconds
                                        pathTransitionDuration: 0.5,

                                        // Can specify path transition in more detail, or remove it entirely
                                        // pathTransition: 'none',

                                        // Colors
                                        pathColor: `rgba(227, 2, 85, 1)`,
                                        textColor: '#3f3f3f',
                                        trailColor: 'white',
                                        backgroundColor: '#e8e8e8',
                                    })}
                                />
                            </div>
                            <span style={{paddingTop: '15px', fontWeight: 'bold', fontSize: '1.3rem'}}>
                                مقالات با همکاری بین الملل
                            </span>
                        </Col>

                        <Col className={'col-3 d-flex justify-content-center flex-column align-items-center'}>
                            <div style={{width: '50%', fontWeight: 'bold'}}>
                                <CircularProgressbar
                                    value={10}
                                    text={`${10}%`}
                                    background
                                    styles={buildStyles({
                                        // Rotation of path and trail, in number of turns (0-1)
                                        rotation: 1,

                                        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                                        strokeLinecap: 'butt',

                                        // Text size
                                        textSize: '1.8rem',
                                        // How long animation takes to go from one percentage to another, in seconds
                                        pathTransitionDuration: 0.5,

                                        // Can specify path transition in more detail, or remove it entirely
                                        // pathTransition: 'none',

                                        // Colors
                                        pathColor: `rgba(227, 2, 85, 1)`,
                                        textColor: '#3f3f3f',
                                        trailColor: 'white',
                                        backgroundColor: '#e8e8e8',
                                    })}
                                />
                            </div>
                            <span style={{paddingTop: '15px', fontWeight: 'bold', fontSize: '1.3rem'}}>
                                خود استنادی (دانشگاه)
                            </span>
                        </Col>

                        <Col className={'col-3 d-flex justify-content-center flex-column align-items-center'}>
                            <div style={{width: '50%', fontWeight: 'bold'}}>
                                <CircularProgressbar
                                    value={70}
                                    text={`${70}%`}
                                    background
                                    styles={buildStyles({
                                        // Rotation of path and trail, in number of turns (0-1)
                                        rotation: 1,

                                        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                                        strokeLinecap: 'butt',

                                        // Text size
                                        textSize: '1.8rem',
                                        // How long animation takes to go from one percentage to another, in seconds
                                        pathTransitionDuration: 0.5,

                                        // Can specify path transition in more detail, or remove it entirely
                                        // pathTransition: 'none',

                                        // Colors
                                        pathColor: `rgba(227, 2, 85, 1)`,
                                        textColor: '#3f3f3f',
                                        trailColor: 'white',
                                        backgroundColor: '#e8e8e8',
                                    })}
                                />
                            </div>
                            <span style={{paddingTop: '15px', fontWeight: 'bold', fontSize: '1.3rem'}}>
                               اعضای هیات علمی با صفر یا یک استناد
                            </span>
                        </Col>

                        <Col className={'col-3 d-flex justify-content-center flex-column align-items-center'}>
                            <div style={{width: '50%', fontWeight: 'bold'}}>
                                <CircularProgressbar
                                    value={37}
                                    text={`${37}%`}
                                    background
                                    styles={buildStyles({
                                        // Rotation of path and trail, in number of turns (0-1)
                                        rotation: 1,

                                        // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                                        strokeLinecap: 'butt',

                                        // Text size
                                        textSize: '1.8rem',
                                        // How long animation takes to go from one percentage to another, in seconds
                                        pathTransitionDuration: 0.5,

                                        // Can specify path transition in more detail, or remove it entirely
                                        // pathTransition: 'none',

                                        // Colors
                                        pathColor: `rgba(227, 2, 85, 1)`,
                                        textColor: '#3f3f3f',
                                        trailColor: 'white',
                                        backgroundColor: '#e8e8e8',
                                    })}
                                />
                            </div>
                            <span style={{paddingTop: '15px', fontWeight: 'bold', fontSize: '1.3rem'}}>
                               مقالات i10
                            </span>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: "30px"}}>
                    <span style={{fontSize: '2rem', fontWeight: "bold"}}>
                        زمینه های پژوهشی مقالات
                    </span>
                </Col>
                <Col className={'col-12 d-flex justify-content-center'}
                     style={{paddingTop: '30px', paddingBottom: "30px", borderBottom: "1px solid #e1e4e6"}}>
                    {/*importing svg as a HTML*/}
                    <div dangerouslySetInnerHTML={{__html: researchFieldSvg}}/>
                </Col>
                <Col className={'col-12 d-flex justify-content-center'} style={{paddingTop: "30px"}}>
                    <Row className={'col-12'}>
                        <Col className={'col-6 d-flex justify-content-start'}>
                        <span style={{fontSize: '2rem', fontWeight: "bold"}}>
                            معرفی
                        </span>
                        </Col>
                        <Col className={'col-6 d-flex justify-content-end'}>
                            <span style={{fontSize: "1.5rem", fontWeight: 'bold'}}>
                                http://www.sharif.ir
                            </span>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12 d-flex justify-content-center'}>
                    <Row className={'col-12'}
                         style={{paddingTop: "15px", paddingBottom: '30px', borderBottom: "1px solid #e1e4e6"}}>
                        <Col className={'col-8 d-flex justify-content-start'} style={{paddingTop: "15px"}}>
                            <span style={{textAlign: 'right'}}>
                                دانشگاه صنعتی شریف در سال ۱۳۴۴ با هدف تربیت و تامین بخشی از نیروهای متخصص موردنیاز کشور، در سطوح بالای علمی تاسیس گردید. این دانشگاه در مقایسه با بسیاری از دانشگاه‌های ایران و جهان دانشگاهی جوان و در حال رشد است. خوشبختانه در طول دوران فعالیت خود توانسته است به صورت یک موسسه علمی پیشرو در صنعت، فناوری‌های روز و علوم کاربردی در عرصه علم، منطقه‌ای و جهانی مطرح و جایگاه ارزشمندی پیدا کند و به عنوان مرکزی برای تبدیل رویاهای نسل‌های ایرانی به واقعیت‌های امروزی همواره مورد تحسین و شگفتی جامعه باشد. هدف این دانشگاه، فراهم نمودن محیطی علمی و پویا برای کسانی است که در راستای کسب علم و دانش تلاش می‌نمایند، و پایگاه و نقطه اتکاء و اعتماد آنانی است که واقعیات و حقایق علمی را درک کرده، برای نشان دادن آن به دیگران کوشش می‌کنند.
                            </span>
                        </Col>
                        <Col className={'col-8 d-flex justify-content-start'} style={{paddingTop: "15px"}}>
                            <span style={{textAlign: 'right'}}>
                                                     دانشگاه صنعتی شریف دیدگاه خویش را در خلق، انتشار و کاربرد دانش در یک محیط یادگیری کاملا پویا
                            جستجو می‌کند. این دیدگاه سبب ارتقای کیفی آموزش، پژوهش، و ارائه خدمات به جامعه و حل مسائل آن
                            در حد توان شده است. بطوری که با بیش از ۴۸ سال تجربه مفتخر است که با یاری خداوند بزرگ و تلاش
                            استادان کارآمد و مجرب، محققین برجسته، و کارشناسان و کارمندان سخت‌کوش و همچنین بهره‌گیری از
                            امکانات کمک‌آموزشی و آزمایشگاه‌های مجهز توانسته است دانشجویان مستعد خویش را در سطح
                            کارآمدترین نیروهای علمی و فنی کشور به جامعه ارائه نماید.
                            </span>

                        </Col>
                        <Col className={'col-8 d-flex justify-content-start'} style={{paddingTop: "15px"}}>
                            <span style={{textAlign: 'right'}}>
                                دانش‌آموختگان این دانشگاه همواره در ردیف قوی‌ترین مهندسین و بالاترین مدیریت‌های علمی، فنی و اجرایی کشور بوده و به علاوه در آزمون‌های متمرکز ورودی کارشناس ارشد از نظر میانگین نمرات و درصد قبولی از کارشناسی به کارشناسی‌ارشد در اکثر رشته‌های فنی، مهندسی و علوم پایه بالاترین مقام را در بین دانشگاه‌های کشور داشته‌اند. همچنین آنهایی که به قصد ادامه تحصیل راهی خارج از کشور شده‌اند نیز به خوبی در سطح بین‌المللی درخشیده‌اند و پس از کسب موفقیت‌های بالای علمی منشاء خدمات ارزنده‌ای گردیده‌اند.
                            </span>
                        </Col>
                        <Col className={'col-8 d-flex justify-content-start'} style={{paddingTop: "15px"}}>
                            <span style={{textAlign: 'right'}}>
                                دانشگاه صنعتی شریف دارای ۴۸۴ عضو هیأت علمی است. از این تعداد ۱۸۱ نفر (۳۷.۴%) در مرتبه استاد تمام، ۱۰۵ نفر (۲۱.۷%) در مرتبه دانشیار، ۱۷۶ نفر در مرتبه استادیار (۳۶.۴%) و ۲۲ نفر به عنوان مربی (۴.۵%)، اعضای هیأت علمی را تشکیل می دهند.
                            </span>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: "30px"}}>
                    <span style={{fontSize: "2rem", fontWeight: 'bold'}}>
                        هرم هیات علمی
                    </span>
                </Col>
                <Col className={'col-12'} style={{borderBottom: "1px solid #e1e4e6"}}>
                    <Row className={'col-12'} style={{paddingBottom: '30px'}}>
                        <Col className={'col-6 d-flex justify-content-end'} style={{paddingLeft: 0}}>
                            <img src={EPRight} alt="EPRight" width={'70%'}/>
                        </Col>
                        <Col className={'col-6 d-flex justify-content-start'} style={{paddingRight: 0}}>
                            <img src={EPLeft} alt="EPLeft" width={'70%'}/>
                        </Col>
                    </Row>
                </Col>

                <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: "30px"}}>
                    <span style={{fontSize: "2rem", fontWeight: 'bold'}}>
                        اعضای عیات علمی
                    </span>
                </Col>
                <Col className={'col-12 d-flex justify-content-center'} style={{paddingBottom: '30px'}}>
                    <Row className={'col-12'} style={{paddingTop: "15px"}}>
                        <Col className={'col-8 d-flex justify-content-start'}>
                            <input type="text" placeholder={'جستجوی نام'}/>
                        </Col>
                        <Col className={'col-4'}>
                            <Row className={'col-12'}>
                                <Col className={'col-6'} style={{paddingLeft: 0, borderLeft: '1px solid #e1e4e6'}}>
                                    <span>
                                        نمایش 19 تا از کل 443 عضو
                                    </span>
                                </Col>
                                <Col className={'col-6'} style={{paddingRight: 0}}>
                                    <span>
                                        مرتب شده بر اساس H-Index
                                    </span>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12'} style={{paddingBottom: "30px", borderBottom: '1px solid #e1e4e6'}}>
                    <Row className={'col-12'}>
                        {facultyList.length > 0 ? facultyList.map((eachMember, index) => (
                                (
                                    //TODO: Add navigator
                                    <Col className={'col-4'} key={index} style={{paddingBottom: '30px'}}>
                                        <Row className={'col-12'}>
                                            <Col className={'col-3'}>
                                                <img src={avatar} alt="avatarImage" width={'100%'}/>
                                            </Col>
                                            <Col className={'col-9'}>
                                                <Col className={'col-12 d-flex justify-content-start'} style={{
                                                    paddingBottom: '10px',
                                                    color: "#215B90",
                                                    fontSize: '1.5rem',
                                                    fontWeight: 'bold'
                                                }}>
                                                    {eachMember.name}
                                                </Col>
                                                <Col className={'col-12 d-flex justify-content-start'}
                                                     style={{paddingBottom: '10px', fontSize: '1.2rem'}}>
                                                    {eachMember.rank + " "}
                                                    دانشکده مهندسی شیمی
                                                </Col>
                                                <Col className={'col-12'}>
                                                    <Row className={'col-12'} style={{paddingBottom: '10px'}}>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, textAlign: "right"}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                H-index: {eachMember.hIndex}
                                                            </span>
                                                        </Col>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, paddingRight: 0, textAlign: 'right'}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                مقالات :
                                                                {eachMember.journals}
                                                            </span>
                                                        </Col>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, paddingRight: 0, textAlign: 'right'}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                استنادات :
                                                                {eachMember.documents}
                                                            </span>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Col>
                                        </Row>
                                    </Col>
                                )
                            ))
                            : "No Faculty Available"}
                    </Row>
                </Col>
                <Col className={'col-12 d-flex justify-content-center'}
                     style={{paddingTop: "30px", paddingBottom: '30px'}}>
                    <Row className={'col-12'}>
                        <Col className={'col-6 d-flex justify-content-start align-items-center'}>
                        <span style={{fontSize: '2rem', fontWeight: "bold"}}>
                            روند مقالات
                            <span style={{color: "grey", paddingRight: '5px'}}>
                                Scopus
                            </span>
                        </span>
                        </Col>
                        <Col className={'col-6 d-flex justify-content-end align-items-center'}>
                            <span style={{fontSize: "1.5rem", fontWeight: 'bold'}}>
                                عدم محاسبه
                            </span>
                            <span className="radioGroup" data-group-name="collection"
                                  style={{width: "90px", paddingRight: '5px'}}>
                                <input type="radio" name="collection" id="collection_1" value="2"/>
                                <label htmlFor="collection_1">خود استنادی</label>
                            </span>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12'}>
                    <div style={{
                        width: '100%',
                        margin: 'auto',
                        display: 'flex',
                        justifyContent: 'center',
                        borderBottom: '1px solid #8d002b'
                    }}>
                        <button
                            className={whichJournalTab === 'journals' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            مقالات
                        </button>
                        <button
                            className={whichJournalTab === 'YearDoc' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            استنادات (چاپ سال)
                        </button>
                        <button
                            className={whichJournalTab === 'DocYearDoc' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            استناد (سال استناد)
                        </button>
                        <button
                            className={whichJournalTab === 'HIndex' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            H-Index
                        </button>
                        <button
                            className={whichJournalTab === 'HSIndex' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            HS-Index
                        </button>
                        <button
                            className={whichJournalTab === 'DocsForJournal' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            استناد به ازای مقاله
                        </button>
                        <button
                            className={whichJournalTab === 'selfDoc' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            % خود استنادی
                        </button>
                        <button
                            className={whichJournalTab === 'firstWriteJournal' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            مقالات نویسنده اول
                        </button>
                        <button
                            className={whichJournalTab === 'firstWriteJournalPercent' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            % مقالات نویسنده اول
                        </button>
                        <button
                            className={whichJournalTab === 'highWrittenJournals' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            مقالات پر نویسنده
                        </button>
                    </div>
                </Col>
                <Col className={'col-12'}
                     style={{paddingTop: '30px', paddingBottom: '30px', borderBottom: '1px solid #e1e4e6'}}>
                    <div style={{width: '100%'}}>
                        <LineChart chartData={userData}/>
                    </div>
                </Col>
                <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: "30px"}}>
                    <span style={{fontSize: "2rem", fontWeight: 'bold'}}>
                        فهرست مقالات
                        <span style={{color: 'grey', marginRight: '5px'}}>
                            Scopus
                        </span>
                    </span>
                </Col>
                <Col className={'col-12'} style={{paddingTop: '30px', paddingBottom: '30px'}}>
                    <Row className={'col-12'}>
                        <Col className={'col-4 d-flex justify-content-start'}>
                            <input type="text" placeholder={'Search Paper Title'}/>
                        </Col>
                        <Col className={'col-8'}>
                            <Row className={'col-12'}>
                                <Col className={'col-4'}>
                                    <select className="selectpicker form-control" title="Document Type">
                                        <option>Brief Report</option>
                                        <option>Brief Report</option>
                                        <option>Brief Report</option>
                                    </select>
                                </Col>
                                <Col className={'col-1'}>
                                    <span className="radioGroup" data-group-name="collection">
                                        <input type="radio" name="collection" id="collection_0" value="0"/>
                                        <label htmlFor="collection_0">
                                            <svg xmlns="http://www.w3.org/2000/svg"
                                                 width="16" height="16"
                                                 fill="currentColor"
                                                 className="bi bi-book"
                                                 viewBox="0 0 16 16">
                                                <path
                                                    d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811V2.828zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492V2.687zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"/>
                                            </svg>
                                        </label>
                                    </span>
                                </Col>
                                <Col className={'col-2'}>
                                    <select className="selectpicker form-control" title="از سال">
                                        <option>2022</option>
                                        <option>2022</option>
                                        <option>2022</option>
                                    </select>
                                </Col>
                                <Col className={'col-2'}>
                                    <select className="selectpicker form-control" title="تا سال">
                                        <option>2022</option>
                                        <option>2022</option>
                                        <option>2022</option>
                                    </select>
                                </Col>
                                <Col className={'col-3 d-flex align-items-center'}>
                                    <span>
                                        1 تا 10 از کل 34853 مقاله
                                    </span>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12'}
                     style={{direction: 'ltr', borderBottom: '1px solid #e1e4e6', paddingBottom: '30px'}}>
                    <Table striped hover>
                        <thead style={{borderBottom: "2.2px solid #aabcff", backgroundColor: "#ecf0ff"}}>
                        <tr>
                            <th>No.</th>
                            <th>Title</th>
                            <th>Authors</th>
                            <th>Journal</th>
                            <th>IF</th>
                            <th>SJR</th>
                            <th>CiteScore</th>
                            <th>Published</th>
                            <th>Cited By</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td style={{color: "blue"}}>Explainable automated seizure detection using attentive deep
                                multi-view networks
                            </td>
                            <td>Paknahad, Abasi, A,A., Glenn, M.,Ghorbanzade</td>
                            <td>Biomedical Signal and Processing and Control</td>
                            <td>5.076</td>
                            <td>1.211</td>
                            <td>6.9</td>
                            <td>2023</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td style={{color: "blue"}}>Explainable automated seizure detection using attentive deep
                                multi-view networks
                            </td>
                            <td>Paknahad, Abasi, A,A., Glenn, M.,Ghorbanzade</td>
                            <td>Biomedical Signal and Processing and Control</td>
                            <td>5.076</td>
                            <td>1.211</td>
                            <td>6.9</td>
                            <td>2023</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td style={{color: "blue"}}>Explainable automated seizure detection using attentive deep
                                multi-view networks
                            </td>
                            <td>Paknahad, Abasi, A,A., Glenn, M.,Ghorbanzade</td>
                            <td>Biomedical Signal and Processing and Control</td>
                            <td>5.076</td>
                            <td>1.211</td>
                            <td>6.9</td>
                            <td>2023</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td style={{color: "blue"}}>Explainable automated seizure detection using attentive deep
                                multi-view networks
                            </td>
                            <td>Paknahad, Abasi, A,A., Glenn, M.,Ghorbanzade</td>
                            <td>Biomedical Signal and Processing and Control</td>
                            <td>5.076</td>
                            <td>1.211</td>
                            <td>6.9</td>
                            <td>2023</td>
                            <td>0</td>
                        </tr>
                        </tbody>
                    </Table>
                </Col>
                <Col className={'col-12 d-flex justify-content-center'}
                     style={{paddingTop: "30px", paddingBottom: '30px'}}>
                    <Row className={'col-12'}>
                        <Col className={'col-6 d-flex justify-content-start align-items-center'}>
                        <span style={{fontSize: '2rem', fontWeight: "bold"}}>
                            مقالات منتشر شده در مجلات برتر
                            <span style={{color: "grey", paddingRight: '5px'}}>
                                Scopus
                            </span>
                        </span>
                        </Col>
                        <Col className={'col-6 d-flex justify-content-start align-items-center'}
                             style={{direction: 'ltr'}}>
                            <span style={{fontSize: "1.5rem", fontWeight: 'bold', marginRight: '10px'}}>
                                Criterion:
                            </span>
                            <span className="radioGroup" data-group-name="collection"
                                  style={{paddingRight: '5px'}}>
                                <input type="radio" name="collection" id="collection_1" value="2"/>
                                <label htmlFor="collection_1">CiteScore</label>
                                <input type="radio" name="collection" id="collection_1" value="2"/>
                                <label htmlFor="collection_1">SNIP</label>
                                <input type="radio" name="collection" id="collection_1" value="2"/>
                                <label htmlFor="collection_1">SJR</label>
                            </span>
                        </Col>
                    </Row>
                </Col>
                <Col className={'col-12'}>
                    <div style={{
                        width: '100%',
                        margin: 'auto',
                        display: 'flex',
                        justifyContent: 'end',
                        borderBottom: '1px solid #8d002b'
                    }}>
                        <button
                            className={whichJournalTab === 'journals' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            CiteScore Top 1%
                        </button>
                        <button
                            className={whichJournalTab === 'YearDoc' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            CiteScore Top 5%
                        </button>
                        <button
                            className={whichJournalTab === 'DocYearDoc' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            CiteScore Top 10%
                        </button>
                        <button
                            className={whichJournalTab === 'HIndex' ? 'journalTabsButtonActive journalTabsButton' : 'journalTabsButton'}>
                            CiteScore Top 25%
                        </button>
                    </div>
                </Col>
                <Col className={'col-12'}
                     style={{paddingTop: '30px', paddingBottom: '30px', borderBottom: '1px solid #e1e4e6'}}>
                    <div style={{width: '100%'}}>
                        <LineChart chartData={userData}/>
                    </div>
                </Col>
                <Col className={'col-12 d-flex justify-content-start'}
                     style={{paddingTop: "30px", paddingBottom: '30px'}}>
                    <span style={{fontSize: "2rem", fontWeight: 'bold'}}>
                        دانشکده ها
                    </span>
                </Col>
                <Col className={'col-12'} style={{paddingBottom: "30px", borderBottom: '1px solid #e1e4e6'}}>
                    <Row className={'col-12'}>
                        {facultyList.length > 0 ? facultyList.map((eachMember, index) => (
                                (
                                    //TODO: Add navigator
                                    <Col className={'col-4'} key={index} style={{paddingBottom: '30px'}}>
                                        <Row className={'col-12'}>
                                            <Col className={'col-3'}>
                                                <img src={noLogo} alt="avatarImage" width={'100%'}/>
                                            </Col>
                                            <Col className={'col-9'}>
                                                <Col className={'col-12 d-flex justify-content-start'} style={{
                                                    paddingBottom: '10px',
                                                    color: "#215B90",
                                                    fontSize: '1.5rem',
                                                    fontWeight: 'bold'
                                                }}>
                                                    دانشکده مهندسی کامپیوتر
                                                </Col>
                                                <Col className={'col-12'}>
                                                    <Row className={'col-12'} style={{paddingBottom: '10px'}}>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, textAlign: "right"}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                اعضا:
                                                                33
                                                            </span>
                                                        </Col>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, paddingRight: 0, textAlign: 'right'}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                مقالات :
                                                                {eachMember.journals}
                                                            </span>
                                                        </Col>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, paddingRight: 0, textAlign: 'right'}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                استنادات :
                                                                {eachMember.documents}
                                                            </span>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Col>
                                        </Row>
                                    </Col>
                                )
                            ))
                            : "No Faculty Available"}
                    </Row>
                </Col>
                <Col className={'col-12 d-flex justify-content-start'}
                     style={{paddingTop: "30px", paddingBottom: '30px'}}>
                    <span style={{fontSize: "2rem", fontWeight: 'bold'}}>
                        پژوهشکده ها
                    </span>
                </Col>
                <Col className={'col-12'} style={{paddingBottom: "30px", borderBottom: '1px solid #e1e4e6'}}>
                    <Row className={'col-12'}>
                        {facultyList.length > 0 ? facultyList.map((eachMember, index) => (
                                (
                                    //TODO: Add navigator
                                    <Col className={'col-4'} key={index} style={{paddingBottom: '30px'}}>
                                        <Row className={'col-12'}>
                                            <Col className={'col-3'}>
                                                <img src={noLogo} alt="avatarImage" width={'100%'}/>
                                            </Col>
                                            <Col className={'col-9'}>
                                                <Col className={'col-12 d-flex justify-content-start'} style={{
                                                    paddingBottom: '10px',
                                                    color: "#215B90",
                                                    fontSize: '1.5rem',
                                                    fontWeight: 'bold',
                                                    textAlign: 'right'
                                                }}>
                                                    پژوهشکده توسعه علوم و فناوری فضا
                                                </Col>
                                                <Col className={'col-12'}>
                                                    <Row className={'col-12'} style={{paddingBottom: '10px'}}>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, textAlign: "right"}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                اعضا:
                                                                33
                                                            </span>
                                                        </Col>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, paddingRight: 0, textAlign: 'right'}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                مقالات :
                                                                {eachMember.journals}
                                                            </span>
                                                        </Col>
                                                        <Col className={'col-4'}
                                                             style={{paddingLeft: 0, paddingRight: 0, textAlign: 'right'}}>
                                                            <span style={{fontWeight: 'bold'}}>
                                                                استنادات :
                                                                {eachMember.documents}
                                                            </span>
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Col>
                                        </Row>
                                    </Col>
                                )
                            ))
                            : "No Faculty Available"}
                    </Row>
                </Col>
            </Row>
            {/*<HomeFooter/>*/}
        </div>

    );
}

export default UniversityBody;
