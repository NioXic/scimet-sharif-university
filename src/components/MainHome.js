import '../App.css';
import HomePage from "./Home/HomePage";
import HomeFooter from "./Home/HomeFooter";

function MainHome() {
    return (
        <div className="app mainBackgroundImage">
            <div className="appBody">
                <HomePage/>
            </div>
            {/*<HomeFooter/>*/}
        </div>

    );
}

export default MainHome;
