import '../App.css';
import FacultyMemberHeader from "./FacultyMember/FacultyMemberHeader";
import FacultyMemberBody from "./FacultyMember/FacultyMemberBody";

function MainFacultyMember() {
    return (
        <div className="app d-flex justify-content-center" style={{flexDirection: 'column'}}>
            <FacultyMemberHeader/>
            <FacultyMemberBody/>
        </div>

    );
}

export default MainFacultyMember;
