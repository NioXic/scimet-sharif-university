import {Row, Col, Container, Button} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import {useNavigate} from "react-router-dom";

import logo from '../../assets/secondLogo.png'
import facultyMemberLogo from '../../assets/facultyLogo.png'
import avatar from '../../assets/avatar.jpg'

//styles
import '../../App.css'
import globalStyles from "../globalStyles";
import Trend from "react-trend";

function FacultyMemberHeader() {
    const navigate = useNavigate();

    function pushToUniversityPage() {
        navigate("/university");
    }


    return (
        <div style={{width: "100%"}}>
            <div style={{width: "80%", margin: "auto", paddingBottom: "20px"}}>
                <Row className={'col-12'} style={{paddingTop: '15px', marginRight: "10px"}}>
                    <Col className={'col-6 d-flex justify-content-start align-items-center'}>
                        <img src={logo} className={'emblemClass'} style={{marginLeft: '15px'}}/>
                        <Row>
                            <Col className={'col-12 d-flex justify-content-start mottoTitle align-items-end'}>
                                <span>
                                    دانشگاه صنعتی شریف
                                </span>
                            </Col>
                            <Col className={'col-12 d-flex justify-content-start mottoTitle'}>
                                <span style={{color: "gray", fontSize: "1.5rem"}}>
                                    سامانه علم سنجی دانشگاه
                                </span>
                            </Col>
                        </Row>
                    </Col>
                    <Col className={'col-6 d-flex justify-content-end align-items-center'}>
                        <img src={facultyMemberLogo} className={'emblemClass'}/>
                    </Col>
                </Row>
            </div>
            <Row className={'col-12 secondHeaderSection d-flex justify-content-center'} style={{margin: "auto"}}>
                <Row style={{width: "90%"}}>
                    <Col className={'col-12'}>
                        <Row className={'col-12'} style={{borderBottom: '1px solid #e1e4e6' ,borderTop: '1px solid #e1e4e6'}}>
                            <Col className={'col-11'} style={{paddingLeft: 0}}>
                                <Row className={'col-12'} style={{padding: '15px', paddingLeft: 0, margin: 0}}>
                                    <Col className={'col-2'}>
                                        <img src={avatar} alt="avatarImage" width={'100%'}/>
                                    </Col>
                                    <Col className={'col-8'}
                                         style={{borderLeft: '1px solid lightGrey', paddingLeft: 0}}>
                                        <Row className={'col-12'} style={{margin: 0}}>
                                            <Col className={'col-12'}>
                                                <Row className={'col-12'}
                                                     style={{paddingBottom: '5px'}}>
                                                    <Col className={'col-6 d-flex justify-content-start'}>
                                                        <span style={{fontWeight: 'bold', fontSize: '1.8rem'}}>
                                                            امید اخوان اشرفیه امید
                                                        </span>
                                                    </Col>
                                                    <Col className={'col-6 d-flex justify-content-end'}>
                                                        <span style={{fontSize: '1.8rem'}}>
                                                            Omid Akhavan
                                                        </span>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col className={'col-12'} style={{paddingTop: '5px'}}>
                                                <Row className={'col-12'}>
                                                    <Col className={'col-6 d-flex justify-content-start'}>
                                                        <span className={'facultyMemberSubDetailText'}>
                                                            دانشیار فیزیک
                                                        </span>
                                                    </Col>
                                                    <Col className={'col-6 d-flex justify-content-end'}>
                                                        <span className={'facultyMemberSubDetailText'}>
                                                            Physics
                                                        </span>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col className={'col-12'} style={{paddingTop: '5px'}}>
                                                <Row className={'col-12'}>
                                                    <Col className={'col-6 d-flex justify-content-start'}>
                                                        <span className={'facultyMemberSubDetailText'}>
                                                            دانشکده فیزیک
                                                        </span>
                                                    </Col>
                                                    <Col className={'col-6 d-flex justify-content-end'}>
                                                        <span className={'facultyMemberSubDetailText'}>
                                                            Physics Faculty
                                                        </span>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col className={'col-12'}
                                                 style={{
                                                     paddingTop: '5px',
                                                     borderBottom: "1px solid lightGrey",
                                                     paddingBottom: "2%"
                                                 }}>
                                                <Row className={'col-12'}>
                                                    <Col className={'col-6 d-flex justify-content-start'}>
                                                        <span className={'facultyMemberSubDetailText'}>
                                                            دانشگاه صنعتی شریف
                                                        </span>
                                                    </Col>
                                                    <Col className={'col-6 d-flex justify-content-end'}>
                                                        <span className={'facultyMemberSubDetailText'}>
                                                            Sharif University of Technology
                                                        </span>
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col className={'col-12'} style={{paddingLeft: 0}}>
                                                <Row className={'col-12 d-flex justify-content-start'}
                                                     style={{margin: 0, direction: "ltr", paddingTop: '10px'}}>
                                                    <Col className={'col-12 d-flex justify-content-start'}>
                                                        {/*<Row className={'col-12'} style={{margin: 0}}>*/}
                                                        {/*<Col className={'d-flex align-items-center justify-content-end'} style={{paddingLeft: 0}}>*/}
                                                        <span style={{
                                                            color: '#215B90',
                                                            fontSize: '1.1rem',
                                                            marginRight: "10px"
                                                        }}>
                                                                    <span
                                                                        style={{marginRight: '5px', marginTop: "2px"}}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="14" height="14" fill="currentColor"
                                                                             className="bi bi-box-arrow-up-right"
                                                                             viewBox="0 0 16 16">
                                                                          <path fill-rule="evenodd"
                                                                                d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
                                                                          <path fill-rule="evenodd"
                                                                                d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
                                                                        </svg>
                                                                    </span>
                                                                    Scopus
                                                                </span>
                                                        {/*</Col>*/}
                                                        {/*<Col className={'d-flex align-items-center justify-content-end'}>*/}
                                                        <span style={{
                                                            color: '#215B90',
                                                            fontSize: '1.1rem',
                                                            marginRight: "10px"
                                                        }}>
                                                                    <span
                                                                        style={{marginRight: '5px', marginTop: "2px"}}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="14" height="14" fill="currentColor"
                                                                             className="bi bi-box-arrow-up-right"
                                                                             viewBox="0 0 16 16">
                                                                          <path fill-rule="evenodd"
                                                                                d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
                                                                          <path fill-rule="evenodd"
                                                                                d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
                                                                        </svg>
                                                                    </span>
                                                                    Google Scholar
                                                                </span>
                                                        {/*</Col>*/}
                                                        {/*<Col className={'d-flex align-items-center justify-content-end'}>*/}
                                                        <span style={{
                                                            color: '#215B90',
                                                            fontSize: '1.1rem',
                                                            marginRight: "10px"
                                                        }}>
                                                                    <span
                                                                        style={{marginRight: '5px', marginTop: "2px"}}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="14" height="14" fill="currentColor"
                                                                             className="bi bi-box-arrow-up-right"
                                                                             viewBox="0 0 16 16">
                                                                          <path fill-rule="evenodd"
                                                                                d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
                                                                          <path fill-rule="evenodd"
                                                                                d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
                                                                        </svg>
                                                                    </span>
                                                                    ResearcherID
                                                                </span>
                                                        {/*</Col>*/}
                                                        {/*<Col className={'d-flex align-items-center justify-content-end'}>*/}
                                                        <span style={{
                                                            color: '#215B90',
                                                            fontSize: '1.1rem',
                                                            marginRight: "10px"
                                                        }}>
                                                                    <span
                                                                        style={{marginRight: '5px', marginTop: "2px"}}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="14" height="14" fill="currentColor"
                                                                             className="bi bi-box-arrow-up-right"
                                                                             viewBox="0 0 16 16">
                                                                          <path fill-rule="evenodd"
                                                                                d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
                                                                          <path fill-rule="evenodd"
                                                                                d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
                                                                        </svg>
                                                                    </span>
                                                                    ORCID
                                                                </span>
                                                        {/*</Col>*/}
                                                        {/*<Col className={'d-flex align-items-center justify-content-end'}>*/}
                                                        <span style={{
                                                            color: '#215B90',
                                                            fontSize: '1.1rem',
                                                            marginRight: "10px"
                                                        }}>
                                                                    <span
                                                                        style={{marginRight: '5px', marginTop: "2px"}}>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="14" height="14" fill="currentColor"
                                                                             className="bi bi-box-arrow-up-right"
                                                                             viewBox="0 0 16 16">
                                                                          <path fill-rule="evenodd"
                                                                                d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z"/>
                                                                          <path fill-rule="evenodd"
                                                                                d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z"/>
                                                                        </svg>
                                                                    </span>
                                                                    ResearchGate
                                                                </span>
                                                        {/*</Col>*/}
                                                        {/**/}
                                                        {/*</Row>*/}
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col className={'col-2'} style={{borderLeft: '1px solid lightGrey'}}>
                                        <Col className={'col-12'} style={{paddingBottom: '40%'}}>
                                            <span style={{fontSize: '3rem', fontWeight: "bold"}}>
                                                573622
                                            </span>
                                            <br/>
                                            <span style={{fontSize: "1.5rem"}}>
                                                 استنادات
                                            </span>
                                        </Col>
                                        <Col className={'col-12'}>
                                            <Trend
                                                smooth
                                                autoDraw
                                                autoDrawDuration={1500}
                                                autoDrawEasing="ease-out"
                                                data={[5, 0, 2, 3, 6, 10, 12, 5, 9, 19, 0, 9, 5]}
                                                gradient={['#00c6ff', '#F0F', '#FF0']}
                                                radius={5}
                                                strokeWidth={7.5}
                                                strokeLinecap={'butt'}
                                            />
                                        </Col>
                                    </Col>
                                </Row>
                            </Col>
                            <Col className={'col-1'}>
                                <Row className={'col-12'}>
                                    <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: '15px'}}>
                                        <span style={{color: '#215B90', fontWeight: "bold", fontSize: '1.1rem '}}>
                                            <span style={{marginLeft: '2px'}}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor" className="bi bi-box-arrow-in-right"
                                                     viewBox="0 0 16 16">
                                                  <path fill-rule="evenodd"
                                                        d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z"/>
                                                  <path fill-rule="evenodd"
                                                        d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z"/>
                                                </svg>
                                            </span>
                                            ورود
                                        </span>
                                    </Col>
                                    <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: '3px'}}>
                                        <span style={{color: '#215B90', fontWeight: "bold", fontSize: '1.1rem '}}>
                                            <span style={{marginLeft: '2px'}}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                                     fill="currentColor" className="bi bi-pencil-fill"
                                                     viewBox="0 0 16 16">
                                                  <path
                                                      d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                                </svg>
                                            </span>
                                            اصلاح
                                        </span>
                                    </Col>
                                    <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: '3px'}}>
                                        <span style={{color: '#215B90', fontWeight: "bold", fontSize: '1.1rem '}}>
                                            <span style={{marginLeft: '2px'}}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor"
                                                     className="bi bi-arrow-clockwise" viewBox="0 0 16 16">
                                                  <path fill-rule="evenodd"
                                                        d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
                                                  <path
                                                      d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
                                                </svg>
                                            </span>
                                            بروزرسانی
                                        </span>
                                    </Col>
                                    <Col className={'col-12 d-flex justify-content-start'} style={{paddingTop: '3px'}}>
                                        <span style={{color: '#215B90', fontWeight: "bold", fontSize: '1.1rem '}}>
                                            <span style={{marginLeft: '2px'}}>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16"
                                                     fill="currentColor"
                                                     className="bi bi-printer-fill" viewBox="0 0 16 16">
                                                  <path
                                                      d="M5 1a2 2 0 0 0-2 2v1h10V3a2 2 0 0 0-2-2H5zm6 8H5a1 1 0 0 0-1 1v3a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1v-3a1 1 0 0 0-1-1z"/>
                                                  <path
                                                      d="M0 7a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-1v-2a2 2 0 0 0-2-2H5a2 2 0 0 0-2 2v2H2a2 2 0 0 1-2-2V7zm2.5 1a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                                                </svg>
                                            </span>
                                            چاپ پروفایل
                                        </span>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                    <Col className={'col-12'}>
                        <Row className={'col-12'}>
                            <Col className={'col-6'} style={{paddingTop :'10px', paddingBottom: '10px'}}>
                                <span
                                    style={{fontSize: '1.2rem', color: "#666", fontWeight: 'bold', marginLeft: '10px'}}>
                                    <span style={{marginLeft: "5px"}}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                                             style={{color: '#215B90'}}
                                             fill="currentColor" className="bi bi-boxes" viewBox="0 0 16 16">
                                          <path
                                              d="M7.752.066a.5.5 0 0 1 .496 0l3.75 2.143a.5.5 0 0 1 .252.434v3.995l3.498 2A.5.5 0 0 1 16 9.07v4.286a.5.5 0 0 1-.252.434l-3.75 2.143a.5.5 0 0 1-.496 0l-3.502-2-3.502 2.001a.5.5 0 0 1-.496 0l-3.75-2.143A.5.5 0 0 1 0 13.357V9.071a.5.5 0 0 1 .252-.434L3.75 6.638V2.643a.5.5 0 0 1 .252-.434L7.752.066ZM4.25 7.504 1.508 9.071l2.742 1.567 2.742-1.567L4.25 7.504ZM7.5 9.933l-2.75 1.571v3.134l2.75-1.571V9.933Zm1 3.134 2.75 1.571v-3.134L8.5 9.933v3.134Zm.508-3.996 2.742 1.567 2.742-1.567-2.742-1.567-2.742 1.567Zm2.242-2.433V3.504L8.5 5.076V8.21l2.75-1.572ZM7.5 8.21V5.076L4.75 3.504v3.134L7.5 8.21ZM5.258 2.643 8 4.21l2.742-1.567L8 1.076 5.258 2.643ZM15 9.933l-2.75 1.571v3.134L15 13.067V9.933ZM3.75 14.638v-3.134L1 9.933v3.134l2.75 1.571Z"/>
                                        </svg>
                                    </span>
                                    <span className={'facultyMemberSectionTitleActive'}>
                                        خلاصه عملکرد
                                    </span>
                                </span>
                                    <span style={{fontSize: '1.2rem', color: "#666", fontWeight: 'bold', marginLeft: '10px'}}>
                                    <span style={{marginLeft: "5px"}}>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-fill"
                                                 style={{color: '#215B90'}}
                                                 viewBox="0 0 16 16">
                                              <path
                                                  d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z"/>
                                            </svg>
                                        </span>
                                    <span>
                                        مقالات
                                    </span>
                                </span>
                                <span style={{fontSize: '1.2rem', color: "#666", fontWeight: 'bold', marginLeft: '10px'}}>
                                    <span style={{marginLeft: "5px"}}>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-globe"
                                             style={{color: '#215B90'}}
                                             viewBox="0 0 16 16">
                                          <path
                                              d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4H4.09zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5h2.49zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5H4.847zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5H4.51zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12H5.145zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5H3.82zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068H8.5zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5h2.49zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4h2.355z"/>
                                        </svg>
                                    </span>
                                    <span>
                                        همکاری بین المللی
                                    </span>
                                </span>
                                <span style={{fontSize: '1.2rem', color: "#666", fontWeight: 'bold', marginLeft: '10px'}}>
                                    <span style={{marginLeft: "5px"}}>
                                          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-people-fill"
                                               style={{color: '#215B90'}}
                                               viewBox="0 0 16 16">
                                          <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                                          <path fill-rule="evenodd"
                                                d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z"/>
                                          <path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z"/>
                                        </svg>
                                    </span>
                                    <span>
                                        نویسندگان همکار
                                    </span>
                                </span>
                            </Col>
                        </Row>
                    </Col>
                </Row>

            </Row>
        </div>
    );
}

export default FacultyMemberHeader;


//Styles
const logoColumn = {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",

};