const researchFieldSvg = `<svg width="800" height="350">
    <g transform="translate(400,175)">
        <text text-anchor="middle" transform="translate(117,33)rotate(0)"
              style="font-size: 54px; opacity: 1; font-family: Impact; fill: rgb(57, 59, 121); cursor: default;">
            optimization
        </text>
        <text text-anchor="middle" transform="translate(155,75)rotate(0)"
              style="font-size: 49px; opacity: 1; font-family: Impact; fill: rgb(82, 84, 163); cursor: default;">
            computer simulation
        </text>
        <text text-anchor="middle" transform="translate(-126,40)rotate(0)"
              style="font-size: 43px; opacity: 1; font-family: Impact; fill: rgb(107, 110, 207); cursor: default;">
            algorithms
        </text>
        <text text-anchor="middle" transform="translate(-79,-47)rotate(0)"
              style="font-size: 40px; opacity: 1; font-family: Impact; fill: rgb(156, 158, 222);">finite element method
        </text>
        <text text-anchor="middle" transform="translate(-91,-85)rotate(0)"
              style="font-size: 38px; opacity: 1; font-family: Impact; fill: rgb(99, 121, 57); cursor: default;">
            scanning electron microscopy
        </text>
        <text text-anchor="middle" transform="translate(103,107)rotate(0)"
              style="font-size: 37px; opacity: 1; font-family: Impact; fill: rgb(140, 162, 82); cursor: default;">
            mathematical models
        </text>
        <text text-anchor="middle" transform="translate(-168,86)rotate(0)"
              style="font-size: 33px; opacity: 1; font-family: Impact; fill: rgb(181, 207, 107);">nanoparticles
        </text>
        <text text-anchor="middle" transform="translate(-363,24)rotate(90)"
              style="font-size: 29px; opacity: 1; font-family: Impact; fill: rgb(206, 219, 156); cursor: default;">
            mechanical properties
        </text>
        <text text-anchor="middle" transform="translate(264,-82)rotate(90)"
              style="font-size: 27px; opacity: 1; font-family: Impact; fill: rgb(140, 109, 49);">x-ray diffraction
        </text>
        <text text-anchor="middle" transform="translate(97,132)rotate(0)"
              style="font-size: 27px; opacity: 1; font-family: Impact; fill: rgb(189, 158, 57);">genetic algorithms
        </text>
        <text text-anchor="middle" transform="translate(-54,-9)rotate(0)"
              style="font-size: 26px; opacity: 1; font-family: Impact; fill: rgb(231, 186, 82);">microstructure
        </text>
        <text text-anchor="middle" transform="translate(-182,112)rotate(0)"
              style="font-size: 25px; opacity: 1; font-family: Impact; fill: rgb(231, 203, 148);">numerical methods
        </text>
        <text text-anchor="middle" transform="translate(334,-53)rotate(90)"
              style="font-size: 24px; opacity: 1; font-family: Impact; fill: rgb(132, 60, 57);">nanocomposites
        </text>
        <text text-anchor="middle" transform="translate(203,-91)rotate(90)"
              style="font-size: 24px; opacity: 1; font-family: Impact; fill: rgb(173, 73, 74); cursor: default;">neural
            networks
        </text>
        <text text-anchor="middle" transform="translate(90,-22)rotate(0)"
              style="font-size: 24px; opacity: 1; font-family: Impact; fill: rgb(214, 97, 107); cursor: default;">
            graphene
        </text>
        <text text-anchor="middle" transform="translate(-252,9)rotate(90)"
              style="font-size: 24px; opacity: 1; font-family: Impact; fill: rgb(231, 150, 156); cursor: default;">
            reliability
        </text>
        <text text-anchor="middle" transform="translate(-77,-120)rotate(0)"
              style="font-size: 22px; opacity: 1; font-family: Impact; fill: rgb(123, 65, 115);">sensitivity analysis
        </text>
        <text text-anchor="middle" transform="translate(73,-117)rotate(0)"
              style="font-size: 22px; opacity: 1; font-family: Impact; fill: rgb(165, 81, 148);">simulation
        </text>
        <text text-anchor="middle" transform="translate(63,-140)rotate(0)"
              style="font-size: 21px; opacity: 1; font-family: Impact; fill: rgb(206, 109, 189);">particle size
        </text>
        <text text-anchor="middle" transform="translate(298,-49)rotate(90)"
              style="font-size: 21px; opacity: 1; font-family: Impact; fill: rgb(222, 158, 214);">signal processing
        </text>
        <text text-anchor="middle" transform="translate(-70,135)rotate(0)"
              style="font-size: 21px; opacity: 1; font-family: Impact; fill: rgb(57, 59, 121);">adsorption
        </text>
        <text text-anchor="middle" transform="translate(-55,-142)rotate(0)"
              style="font-size: 20px; opacity: 1; font-family: Impact; fill: rgb(82, 84, 163);">heat transfer
        </text>
        <text text-anchor="middle" transform="translate(-177,134)rotate(0)"
              style="font-size: 20px; opacity: 1; font-family: Impact; fill: rgb(107, 110, 207);">deformation
        </text>
        <text text-anchor="middle" transform="translate(-298,-120)rotate(0)"
              style="font-size: 20px; opacity: 1; font-family: Impact; fill: rgb(156, 158, 222); cursor: default;">
            temperature
        </text>
        <text text-anchor="middle" transform="translate(-254,-140)rotate(0)"
              style="font-size: 19px; opacity: 1; font-family: Impact; fill: rgb(99, 121, 57);">computational fluid
            dynamics
        </text>
        <text text-anchor="middle" transform="translate(200,152)rotate(0)"
              style="font-size: 19px; opacity: 1; font-family: Impact; fill: rgb(140, 162, 82);">problem-solving
        </text>
        <text text-anchor="middle" transform="translate(298,125)rotate(0)"
              style="font-size: 19px; opacity: 1; font-family: Impact; fill: rgb(181, 207, 107);">genetic algorithm
        </text>
        <text text-anchor="middle" transform="translate(40,153)rotate(0)"
              style="font-size: 19px; opacity: 1; font-family: Impact; fill: rgb(206, 219, 156);">titanium dioxide
        </text>
        <text text-anchor="middle" transform="translate(-117,155)rotate(0)"
              style="font-size: 19px; opacity: 1; font-family: Impact; fill: rgb(140, 109, 49);">molecular dynamics
        </text>
        <text text-anchor="middle" transform="translate(-187,1)rotate(0)"
              style="font-size: 19px; opacity: 1; font-family: Impact; fill: rgb(189, 158, 57);">aluminum
        </text>
        <text text-anchor="middle" transform="translate(299,95)rotate(0)"
              style="font-size: 19px; opacity: 1; font-family: Impact; fill: rgb(231, 186, 82);">costs
        </text>
        <text text-anchor="middle" transform="translate(-279,-2)rotate(90)"
              style="font-size: 18px; opacity: 1; font-family: Impact; fill: rgb(231, 203, 148); cursor: default;">
            controllers
        </text>
        <text text-anchor="middle" transform="translate(139,-42)rotate(0)"
              style="font-size: 18px; opacity: 1; font-family: Impact; fill: rgb(132, 60, 57); cursor: default;">design
        </text>
        <text text-anchor="middle" transform="translate(-182,-19)rotate(0)"
              style="font-size: 18px; opacity: 1; font-family: Impact; fill: rgb(173, 73, 74);">algorithm
        </text>
        <text text-anchor="middle" transform="translate(148,-63)rotate(0)"
              style="font-size: 18px; opacity: 1; font-family: Impact; fill: rgb(214, 97, 107); cursor: default;">
            geometry
        </text>
        <text text-anchor="middle" transform="translate(162,-119)rotate(90)"
              style="font-size: 17px; opacity: 1; font-family: Impact; fill: rgb(231, 150, 156);">efficiency
        </text>
        <text text-anchor="middle" transform="translate(329,144)rotate(0)"
              style="font-size: 17px; opacity: 1; font-family: Impact; fill: rgb(123, 65, 115);">biomechanics
        </text>
        <text text-anchor="middle" transform="translate(-253,-159)rotate(0)"
              style="font-size: 17px; opacity: 1; font-family: Impact; fill: rgb(165, 81, 148);">decision-making
        </text>
        <text text-anchor="middle" transform="translate(-300,10)rotate(90)"
              style="font-size: 17px; opacity: 1; font-family: Impact; fill: rgb(206, 109, 189); cursor: default;">
            synthesis (chemical)
        </text>
        <text text-anchor="middle" transform="translate(-271,154)rotate(0)"
              style="font-size: 17px; opacity: 1; font-family: Impact; fill: rgb(222, 158, 214);">porous materials
        </text>
        <text text-anchor="middle" transform="translate(-102,56)rotate(0)"
              style="font-size: 16px; opacity: 1; font-family: Impact; fill: rgb(57, 59, 121);">elasticity
        </text>
        <text text-anchor="middle" transform="translate(145,-158)rotate(0)"
              style="font-size: 16px; opacity: 1; font-family: Impact; fill: rgb(82, 84, 163);">forecasting
        </text>
        <text text-anchor="middle" transform="translate(-36,-31)rotate(0)"
              style="font-size: 16px; opacity: 1; font-family: Impact; fill: rgb(107, 110, 207);">friction
        </text>
        <text text-anchor="middle" transform="translate(-155,-159)rotate(0)"
              style="font-size: 16px; opacity: 1; font-family: Impact; fill: rgb(156, 158, 222);">oxidation
        </text>
        <text text-anchor="middle" transform="translate(229,-76)rotate(90)"
              style="font-size: 16px; opacity: 1; font-family: Impact; fill: rgb(99, 121, 57);">stability
        </text>
        <text text-anchor="middle" transform="translate(-62,8)rotate(0)"
              style="font-size: 16px; opacity: 1; font-family: Impact; fill: rgb(140, 162, 82); cursor: default;">
            kinetics
        </text>
        <text text-anchor="middle" transform="translate(-276,131)rotate(0)"
              style="font-size: 16px; opacity: 1; font-family: Impact; fill: rgb(181, 207, 107);">morphology
        </text>
        <text text-anchor="middle" transform="translate(365,-91)rotate(90)"
              style="font-size: 15px; opacity: 1; font-family: Impact; fill: rgb(206, 219, 156);">porosity
        </text>
        <text text-anchor="middle" transform="translate(336,160)rotate(0)"
              style="font-size: 15px; opacity: 1; font-family: Impact; fill: rgb(140, 109, 49);">carbon dioxide
        </text>
        <text text-anchor="middle" transform="translate(166,-26)rotate(0)"
              style="font-size: 15px; opacity: 1; font-family: Impact; fill: rgb(189, 158, 57);">copper
        </text>
        <text text-anchor="middle" transform="translate(323,-160)rotate(0)"
              style="font-size: 15px; opacity: 1; font-family: Impact; fill: rgb(231, 186, 82);">dynamics
        </text>
        <text text-anchor="middle" transform="translate(325,-142)rotate(0)"
              style="font-size: 15px; opacity: 1; font-family: Impact; fill: rgb(231, 203, 148);">synthesis
        </text>
        <text text-anchor="middle" transform="translate(180,-123)rotate(90)"
              style="font-size: 14px; opacity: 1; font-family: Impact; fill: rgb(132, 60, 57); cursor: default;">
            modeling
        </text>
        <text text-anchor="middle" transform="translate(-203,58)rotate(0)"
              style="font-size: 14px; opacity: 1; font-family: Impact; fill: rgb(173, 73, 74);">diffusion
        </text>
        <text text-anchor="middle" transform="translate(-14,-161)rotate(0)"
              style="font-size: 14px; opacity: 1; font-family: Impact; fill: rgb(214, 97, 107);">carbon
        </text>
        <text text-anchor="middle" transform="translate(-319,50)rotate(90)"
              style="font-size: 14px; opacity: 1; font-family: Impact; fill: rgb(231, 150, 156);">image processing
        </text>
        <text text-anchor="middle" transform="translate(380,-14)rotate(90)"
              style="font-size: 14px; opacity: 1; font-family: Impact; fill: rgb(123, 65, 115);">carbon nanotubes
        </text>
        <text text-anchor="middle" transform="translate(-380,6)rotate(90)"
              style="font-size: 14px; opacity: 1; font-family: Impact; fill: rgb(165, 81, 148); cursor: default;">
            artificial intelligence
        </text>
        <text text-anchor="middle" transform="translate(-195,-115)rotate(0)"
              style="font-size: 13px; opacity: 1; font-family: Impact; fill: rgb(206, 109, 189);">fracture
        </text>
        <text text-anchor="middle" transform="translate(355,89)rotate(0)"
              style="font-size: 13px; opacity: 1; font-family: Impact; fill: rgb(222, 158, 214);">polymers
        </text>
        <text text-anchor="middle" transform="translate(-363,-160)rotate(0)"
              style="font-size: 13px; opacity: 1; font-family: Impact; fill: rgb(57, 59, 121); cursor: default;">
            robotics
        </text>
        <text text-anchor="middle" transform="translate(248,-68)rotate(90)"
              style="font-size: 13px; opacity: 1; font-family: Impact; fill: rgb(82, 84, 163);">monte carlo methods
        </text>
        <text text-anchor="middle" transform="translate(-319,-65)rotate(0)"
              style="font-size: 13px; opacity: 1; font-family: Impact; fill: rgb(107, 110, 207); cursor: default;">water
        </text>
        <text text-anchor="middle" transform="translate(-112,-34)rotate(0)"
              style="font-size: 13px; opacity: 1; font-family: Impact; fill: rgb(156, 158, 222);">electrodes
        </text>
        <text text-anchor="middle" transform="translate(57,-161)rotate(0)"
              style="font-size: 12px; opacity: 1; font-family: Impact; fill: rgb(99, 121, 57);">nanostructures
        </text>
        <text text-anchor="middle" transform="translate(-81,-163)rotate(0)"
              style="font-size: 12px; opacity: 1; font-family: Impact; fill: rgb(140, 162, 82);">nanoparticle
        </text>
        <text text-anchor="middle" transform="translate(362,-9)rotate(90)"
              style="font-size: 12px; opacity: 1; font-family: Impact; fill: rgb(181, 207, 107);">three dimensional
        </text>
        <text text-anchor="middle" transform="translate(-322,-32)rotate(90)"
              style="font-size: 12px; opacity: 1; font-family: Impact; fill: rgb(206, 219, 156); cursor: default;">
            chitosan
        </text>
        <text text-anchor="middle" transform="translate(-283,-66)rotate(90)"
              style="font-size: 12px; opacity: 1; font-family: Impact; fill: rgb(140, 109, 49); cursor: default;">nickel
        </text>
        <text text-anchor="middle" transform="translate(232,-133)rotate(90)"
              style="font-size: 12px; opacity: 1; font-family: Impact; fill: rgb(189, 158, 57);">thin films
        </text>
        <text text-anchor="middle" transform="translate(383,-143)rotate(90)"
              style="font-size: 12px; opacity: 1; font-family: Impact; fill: rgb(231, 186, 82);">kinematics
        </text>
    </g>
</svg>
`;
export  default researchFieldSvg;