import '../App.css';
import HomePage from "./Home/HomePage";
import HomeFooter from "./Home/HomeFooter";
import UniversityHeader from "./University/UniversityHeader";
import UniversityBody from "./University/UniversityBody";

function MainUniversity() {
    return (
        <div className="app d-flex justify-content-center universityBackgroundImage" style={{flexDirection: 'column'}}>
            <UniversityHeader/>
            <UniversityBody/>
        </div>

    );
}

export default MainUniversity;
