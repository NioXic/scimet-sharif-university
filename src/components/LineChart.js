import React from "react";
import { Line } from "react-chartjs-2";
import Chart from 'chart.js/auto';
import {CategoryScale} from 'chart.js';
Chart.register(CategoryScale);

function LineChartTest({ chartData }) {
    return <Line data={chartData} />;
}

export default LineChartTest;