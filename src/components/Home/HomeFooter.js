import HomeHeader from "./HomeHeader";
import {Container, Row, Col} from "react-bootstrap";
import HomeBody from "./HomeBody";
import globalStyles from "../globalStyles";

function HomeFooter() {
    return (
        <div style={{
            width: "100%",
            backgroundColor: "lightgrey",
            direction: "rtl",
            fontSize: "10.5pt",
            // position: 'absolute',
            bottom: 0,
            paddingTop: '45px',
            paddingBottom: "30px",
        }}>
            <Row className={'col-12'}>
                <Col className={'col-3'}>
                    <Col className={'col-12'}>
                        <span style={{color: globalStyles.primaryColor, fontWeight: 'bold'}}>
                            ارسال پیام
                        </span>
                    </Col>
                    <Col className={'col-12'} style={{marginTop: '15px'}}>
                        <span>
                            چنانچه در مشخصات ذکر شده برای عضو هیات علمی (اعم از مشخصات فردی، پروفایل ها و غیره) اطلاعات ناصحیح مشاهده نمودید یا نظری در مورد سامانه و عملکرد آن دارید تقاضامند است ما را در جریان بگذارید.
                        </span>
                    </Col>
                    <Col className={'col-12'} style={{marginTop: '30px'}}>
                        <span style={{color: "blue", fontWeight: "bold"}}>
                            ارسال باز خورد
                        </span>
                    </Col>
                </Col>


                {/*second one*/}
                <Col className={'col-3'}>
                    <Col className={'col-12'} style={{marginBottom: '15px'}}>
                        <span style={{color: globalStyles.primaryColor, fontWeight: 'bold'}}>
                            آمار سامانه
                        </span>
                    </Col>
                    <Col className={'col-12'} style={{marginBottom: '15px'}}>
                        <Row className={'col-12'} style={{borderBottom: "1px solid #9a9898", paddingBottom: "4px"}}>
                            <Col className={'col-6 d-flex justify-content-start'}>
                                <span>
                                    کاربران آنلاین:
                                </span>
                            </Col>
                            <Col className={'col-6 d-flex justify-content-end'}>
                                <span>
                                    1
                                </span>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{borderBottom: "1px solid #9a9898", paddingBottom: "4px"}}>
                            <Col className={'col-10 d-flex justify-content-start'}>
                                <span>
                                    بازدید امروز:
                                </span>
                            </Col>
                            <Col className={'col-2 d-flex justify-content-end'}>
                                <span>
                                    403
                                </span>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{borderBottom: "1px solid #9a9898", paddingBottom: "4px"}}>
                            <Col className={'col-10 d-flex justify-content-start'}>
                                <span>
                                    بازدید کل:
                                </span>
                            </Col>
                            <Col className={'col-2 d-flex justify-content-end'}>
                                <span>
                                    115613
                                </span>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{borderBottom: "1px solid #9a9898", paddingBottom: "4px"}}>
                            <Col className={'col-10 d-flex justify-content-start'}>
                                <span>
                                    دانشکده ها:
                                </span>
                            </Col>
                            <Col className={'col-2 d-flex justify-content-end'}>
                                <span>
                                    18
                                </span>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{borderBottom: "1px solid #9a9898", paddingBottom: "4px"}}>
                            <Col className={'col-10 d-flex justify-content-start'}>
                                <span>
                                    گروه های آموزشی:
                                </span>
                            </Col>
                            <Col className={'col-2 d-flex justify-content-end'}>
                                <span>
                                    6
                                </span>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{borderBottom: "1px solid #9a9898", paddingBottom: "4px"}}>
                            <Col className={'col-10 d-flex justify-content-start'}>
                                <span>
                                    اعضای هیات علمی شاغل:
                                </span>
                            </Col>
                            <Col className={'col-2 d-flex justify-content-end'}>
                                <span>
                                    443
                                </span>
                            </Col>
                        </Row>
                    </Col>
                </Col>
                <Col className={'col-3'}>
                    <Col className={'col-12'} style={{marginBottom: '15px'}}>
                        <span style={{color: globalStyles.primaryColor, fontWeight: 'bold'}}>
                            تماس با ما
                        </span>
                    </Col>
                    <Col className={'col-12'} style={{marginBottom: '15px'}}>
                        <span>
                            ایران، تهران، میدان آزادی، دانشگاه صنعتی شریف، معاونت تحقیقات و فناوری
                            <br/>
                            <br/>
                            پست الکترونیک:vcr@sharifi.ir
                        </span>
                    </Col>
                </Col>

                {/*last one*/}
                <Col className={'col-3'}>
                    <Col className={'col-12'} style={{marginBottom: '15px'}}>
                        <span style={{color: globalStyles.primaryColor, fontWeight: 'bold'}}>
                            توجه
                        </span>
                    </Col>
                    <Col className={'col-12'}>
                        <span>
                            کلیه حقوق این وبسایت و مطالب آن متعلق  به سامانه علم سنجی دانشگاه صنتعی شریف بوده و استفاده از مطالب آن با ذکر منبع بلامانع است.
                            <br/>
                            <br/>
                            2022 SHARIF UNIVERSITY OF TECHNOLOGY.
                        </span>
                    </Col>
                </Col>
            </Row>
        </div>
    );
}

export default HomeFooter;
