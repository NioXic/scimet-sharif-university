import HomeHeader from "./HomeHeader";
import {Container} from "react-bootstrap";
import HomeBody from "./HomeBody";


function HomePage() {
    return (
            <Container style={{width: "100%"}}>
                <HomeHeader/>
                <HomeBody/>
            </Container>

);
}

export default HomePage;
