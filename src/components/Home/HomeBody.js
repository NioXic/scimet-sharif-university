import customFilter from "../../customFilter";
import {Container, Row, Col, Button} from "react-bootstrap";
import "../../App.css"
import Table from 'react-bootstrap/Table';
import {useNavigate} from "react-router-dom";

function HomeBody() {
    const navigate = useNavigate();

    function pushToFacultyProfile() {
        navigate("/omid_akhavan");
    }
    return (
        <Container style={{width: "100%", paddingBottom: "30px"}}>
            <Row className={'col-12'}>
                {/*Faculty Members Row*/}
                <Col className={'col-12 d-flex justify-content-end'}
                     style={{borderBottom: "1px solid lightgrey", paddingBottom: "3px"}}>
                    <span style={{fontWeight: "bold", fontSize: "1.2rem"}}>
                        اعضای هیات علمی:
                        {/*persian digit filter*/}
                        {" " + customFilter.persianDigit(443)}
                    </span>
                </Col>
                {/*    Main Table */}
            </Row>
            <Row className={'d-flex justify-content-end'}>
                <Col style={{paddingTop: '5px'}} className={'col-6 justify-content-start d-flex align-items-center'}>
                   <span style={{fontWeight: "bold", fontSize: "1.2rem", marginLeft: '5px'}}>
                       مجموعه:
                   </span>
                    <span className="radioGroup" data-group-name="collection">
                        <input type="radio" name="collection" id="collection_0" value="0"/>
                        <label htmlFor="collection_0">Scopus</label>
                            <input type="radio" name="collection" id="collection_1" value="2"/>
                            <label htmlFor="collection_1">WoS</label>
                            <input type="radio" name="collection" id="collection_2" value="1"/>
                        <label htmlFor="collection_2">Google Scholar</label>
                    </span>
                </Col>
            </Row>
            <Row className={'d-flex justify-content-center col-12'} style={{paddingTop: '5px'}}>
                <Table striped hover>
                    <thead style={{borderBottom: "2.2px solid #aabcff", backgroundColor: "#ecf0ff"}}>
                    <tr>
                        <th>ردیف</th>
                        <th>
                            <span style={{display: 'flex', alignItems: 'center', justifyContent: "center"}}>
                            نام
                                <span className={'d-flex'} style={{flexDirection: 'column', marginRight: '5px'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                         <path
                                             d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                      <path
                                          d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                    </svg>
                                </span>
                            </span>
                        </th>
                        <th>دانشکده</th>
                        <th>رشته تحصیلی</th>
                        <th>رتبه علمی</th>
                        <th>
                            <span style={{display: 'flex', alignItems: 'center'}}>
                                مقالات
                                <span className={'d-flex'} style={{flexDirection: 'column', marginRight: '5px'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                         <path
                                             d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                      <path
                                          d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                    </svg>
                                </span>
                            </span>
                        </th>
                        <th>
                            <span style={{display: 'flex', alignItems: 'center'}}>
                                استنادات
                                <span className={'d-flex'} style={{flexDirection: 'column', marginRight: '5px'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                         <path
                                             d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                      <path
                                          d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                    </svg>
                                </span>
                            </span>
                        </th>
                        <th>خود استنادی</th>
                        <th>
                            <span style={{display: 'flex', alignItems: 'center'}}>
                                H-Index
                                <span className={'d-flex'} style={{flexDirection: 'column', marginRight: '5px'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                         <path
                                             d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                      <path
                                          d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                    </svg>
                                </span>
                            </span>
                        </th>
                        <th>
                            <span style={{display: 'flex', alignItems: 'center'}}>
                                G-Index
                                <span className={'d-flex'} style={{flexDirection: 'column', marginRight: '5px'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                         <path
                                             d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                      <path
                                          d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                    </svg>
                                </span>
                            </span>
                        </th>
                        <th>
                            <span style={{display: 'flex', alignItems: 'center'}}>
                                استناد بازای مقاله
                                <span className={'d-flex'} style={{flexDirection: 'column', marginRight: '5px'}}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-up-fill" viewBox="0 0 16 16">
                                         <path
                                             d="m7.247 4.86-4.796 5.481c-.566.647-.106 1.659.753 1.659h9.592a1 1 0 0 0 .753-1.659l-4.796-5.48a1 1 0 0 0-1.506 0z"/>
                                    </svg>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="9" height="9" fill="currentColor"
                                         className="bi bi-caret-down-fill" viewBox="0 0 16 16">
                                      <path
                                          d="M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"/>
                                    </svg>
                                </span>
                            </span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr onClick={pushToFacultyProfile}>
                        <td>{customFilter.persianDigit(1)}</td>
                        <td style={{color: "blue"}}>محمدرضا موحدی</td>
                        <td>دانشکده مهندسی مکانیک</td>
                        <td>مهندسی مکانیک</td>
                        <td>استاد</td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                    </tr>
                    <tr onClick={pushToFacultyProfile}>
                        <td>{customFilter.persianDigit(1)}</td>
                        <td style={{color: "blue"}}>محمدرضا موحدی</td>
                        <td>دانشکده مهندسی مکانیک</td>
                        <td>مهندسی مکانیک</td>
                        <td>استاد</td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                    </tr>
                    <tr onClick={pushToFacultyProfile}>
                        <td>{customFilter.persianDigit(1)}</td>
                        <td style={{color: "blue"}}>محمدرضا موحدی</td>
                        <td>دانشکده مهندسی مکانیک</td>
                        <td>مهندسی مکانیک</td>
                        <td>استاد</td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                    </tr>
                    <tr onClick={pushToFacultyProfile}>
                        <td>{customFilter.persianDigit(1)}</td>
                        <td style={{color: "blue"}}>محمدرضا موحدی</td>
                        <td>دانشکده مهندسی مکانیک</td>
                        <td>مهندسی مکانیک</td>
                        <td>استاد</td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                    </tr>
                    <tr onClick={pushToFacultyProfile}>
                        <td>{customFilter.persianDigit(1)}</td>
                        <td style={{color: "blue"}}>محمدرضا موحدی</td>
                        <td>دانشکده مهندسی مکانیک</td>
                        <td>مهندسی مکانیک</td>
                        <td>استاد</td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                        <td>.</td>
                        <td>.</td>
                        <td></td>
                    </tr>
                    </tbody>
                </Table>
                <Row className={'col-12 tableFooterStyle'}>
                    <Col className={'col-6 d-flex justify-content-start align-items-center'}>
                       <span style={{fontWeight: "bold", fontSize: "1.2rem", marginLeft: '5px'}}>
                           برو به صغحه:
                       </span>
                        <span className="radioGroup" data-group-name="collection">
                        <input type="radio" name="collection" id="collection_0" value="0"/>
                        <label htmlFor="collection_0">{customFilter.persianDigit(1)}</label>
                        <input type="radio" name="collection" id="collection_1" value="2"/>
                        <label htmlFor="collection_1">{customFilter.persianDigit(2)}</label>
                        <input type="radio" name="collection" id="collection_2" value="1"/>
                        <label htmlFor="collection_2">{customFilter.persianDigit(3)}</label>
                        <input type="radio" name="collection" id="collection_2" value="1"/>
                        <label htmlFor="collection_2">{customFilter.persianDigit(4)}</label>
                        <input type="radio" name="collection" id="collection_2" value="1"/>
                        <label htmlFor="collection_2">{'>'}</label>
                        <input type="radio" name="collection" id="collection_2" value="1"/>
                        <label htmlFor="collection_2">{'>>'}</label>
                    </span>
                    </Col>
                    <Col className={'col-6 d-flex justify-content-end align-items-center'}>
                       <span style={{fontWeight: "bold", fontSize: "1.2rem", marginLeft: '5px'}}>
                           تعداد در صغحه:
                       </span>
                        <span className="radioGroup" data-group-name="collection">
                        <input type="radio" name="collection" id="collection_0" value="0"/>
                        <label htmlFor="collection_0">{customFilter.persianDigit(5)}</label>
                        <input type="radio" name="collection" id="collection_1" value="2"/>
                        <label htmlFor="collection_1">{customFilter.persianDigit(10)}</label>
                        <input type="radio" name="collection" id="collection_2" value="1"/>
                        <label htmlFor="collection_2">{customFilter.persianDigit(20)}</label>
                        <input type="radio" name="collection" id="collection_2" value="1"/>
                        <label htmlFor="collection_2">{customFilter.persianDigit(50)}</label>
                    </span>
                    </Col>
                </Row>
            </Row>
        </Container>
    );
}

export default HomeBody;
