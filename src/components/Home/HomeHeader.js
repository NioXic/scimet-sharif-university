import {Row, Col, Container, Button} from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import {useNavigate} from "react-router-dom";

import logo from '../../assets/logo.png'
//styles
import '../../App.css'
import globalStyles from "../globalStyles";

function HomeHeader() {
    const navigate = useNavigate();

    function pushToUniversityPage() {
        navigate("/university");
    }


    return (
        <div style={{width: "100%"}}>
            <Row className={'col-12'}>
                <Col className={'col-4'} style={logoColumn}>
                    <img src={logo} style={{width: "60%"}}/>
                    <span style={{
                        fontSize: "1.5rem",
                        fontWeight: 'bold',
                        marginTop: "15px",
                        color: globalStyles.primaryColor
                    }}>
                        معاونت پژوهش و فناوری
                    </span>
                </Col>
                <Col className={'col-6'}>
                    <Container style={{marginTop: "25px", width: "60%"}}>
                        <Row className={'col-12'} style={{paddingBottom: "5px"}}>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{paddingBottom: "5px"}}>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{paddingBottom: "5px"}}>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{paddingBottom: "5px"}}>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                            <Col className={'col-6'}>
                                <select className="form-select"
                                        aria-label="Default select example">
                                    <option>پژوهشکده الکترونیک</option>
                                    <option>دانشکده شیمی</option>
                                    <option>دانشکده فیزیک</option>
                                    <option>علوم ریاضی</option>
                                </select>
                            </Col>
                        </Row>
                        <Row className={'col-12'} style={{paddingBottom: "5px"}}>
                            <Col className={'col-12'}>
                                <Form.Control
                                    placeholder={'نام'}
                                    aria-label="Example text with button addon"
                                    aria-describedby="basic-addon1"
                                />
                            </Col>
                        </Row>
                        <Row className={'col-12 justify-content-end d-flex'}>
                            <Col className={'col-6'} style={{padding: 0}}>
                                <Button style={{
                                    color: globalStyles.primaryColor,
                                    background: "transparent",
                                    width: "45%",
                                    fontWeight: "bold",
                                    border: `1px solid ${globalStyles.primaryColor}`,

                                }}
                                        className={'searchButton'}>
                                    <span>
                                        جستجو
                                    </span>
                                </Button>
                                <Button style={{
                                    color: globalStyles.primaryColor,
                                    background: "transparent",
                                    width: "45%",
                                    marginRight: "3px",
                                    fontWeight: "bold",
                                    border: `1px solid ${globalStyles.primaryColor}`,

                                }}
                                        className={'searchButton'}>
                                    <span>
                                        بازنشانی
                                    </span>
                                </Button>
                            </Col>
                        </Row>
                    </Container>
                </Col>
                <Col className={'col-2 d-flex justify-content-end'}
                     onClick={pushToUniversityPage}
                     style={{
                         cursor: "pointer",
                         paddingTop: "15px",
                         fontWeight: "bold",
                         fontSize: '1.2rem',
                         color: globalStyles.primaryColor
                     }}>
                    <span>
                        پروفایل دانشکاه
                    </span>
                </Col>
            </Row>
        </div>
    );
}

export default HomeHeader;


//Styles
const logoColumn = {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",

};